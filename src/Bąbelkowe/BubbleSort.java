package Bąbelkowe;

public class BubbleSort {

    public void bubbleSort (int[] tab) {
        int n = tab.length;
        for (int j = 0; j < n; j++) {
            for (int i = 0; i < n-1-j; i++) {
                if (tab[i] > tab[i+1]) {
                    int temp = tab[i+1];
                    tab[i+1] = tab[i];
                    tab[i] = temp;
                }
            }
        }
    }
}
