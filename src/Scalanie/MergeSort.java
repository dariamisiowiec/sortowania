package Scalanie;

public class MergeSort {

    public void mergeSort(int[] tab) {
        int n = tab.length;
        if (n<2)
            return;
        int middle = n/2;
        int[] left = new int[middle];
        int[] right = new int[n - middle];
        for (int i = 0; i < middle; i++) {
            left[i] = tab[i];
        }
        int j = 0;
        for (int i = middle; i < n; i++) {
            right[j] = tab[i];
            j++;
        }
        mergeSort(left);
        mergeSort(right);
        merge(left, right, tab);
    }

    public void merge (int[] left, int[] right, int[] whole) {
        int i = 0, j = 0, k = 0;
        while (i<left.length && j<right.length) {
            if (left[i] < right[j]) {
                whole[k] = left[i];
                i++;
            } else {
                whole[k] = right[j];
                j++;
            }
            k++;
        }
        while (i<left.length) {
            whole[k] = left[i];
            k++;
            i++;
        }
        while (j<right.length) {
            whole[k] = right[j];
            k++;
            j++;
        }
    }
}
