package Wybieranie;

public class SelectionSort {

    public void selectionSort(int tab[]) {
        int theSmallest;
        int theSmallestIndex;
        int temp;
        for (int i = 0; i < tab.length; i++) {
            theSmallest = tab[i];
            theSmallestIndex = i;
            for (int j = i + 1; j < tab.length; j++) {
                if (tab[j] < theSmallest) {
                    theSmallest = tab[j];
                    theSmallestIndex = j;
                }
            }
            temp = tab[i];
            tab[i] = theSmallest;
            tab[theSmallestIndex] = temp;
        }
    }
}

