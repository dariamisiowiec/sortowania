package Wybieranie;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] tab = {1, 10, 8, 2, 5, 3, 1, -6, 12, 11, 15, 2, 9, 6};
        SelectionSort ss = new SelectionSort();
        ss.selectionSort(tab);
        System.out.println(Arrays.toString(tab));
    }
}