package Szybkie;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] tab = {1, 10, 8, 2, 5, 3, 1, -6, 12, 11, 15, 2, 9, 6};
        QuickSort qs = new QuickSort();
        qs.quickSort(tab, 0, tab.length-1);
        System.out.println(Arrays.toString(tab));
    }
}
