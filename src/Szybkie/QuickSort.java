package Szybkie;

public class QuickSort {

    public void quickSort(int array[], int start, int end) {
        if (start >= end)
            return;
        int partitionIndex = partition(array, start, end);
        quickSort(array, start, partitionIndex - 1);
        quickSort(array, partitionIndex + 1, end);
    }

    private int partition(int array[], int start, int end) {
        int pivot = array[end];
        int partitionIndex = start;
        for (int i = start; i < end; i++) {
            if (array[i]<pivot) {
                swap(array, i, partitionIndex);
                partitionIndex++;
            }
        }
        swap(array, partitionIndex, end);
        return partitionIndex;
    }

    private void swap(int[] array, int from, int to) {
        int temp = array[from];
        array[from] = array[to];
        array[to] = temp;
    }

}
