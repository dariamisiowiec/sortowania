package Zliczanie;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] tab = {1, 10, 8, 2, 5, 3, 1, 6, 12, 11, 15, 2, 9, 6};
        CountingSort cs = new CountingSort();
        cs.countingSort(tab, 15);
        System.out.println(Arrays.toString(tab));
    }
}
