package Zliczanie;

public class CountingSort {

    public void countingSort(int tab[], int zakres) {
        int[] iloscWystapien = new int[zakres + 1];
        int n = tab.length;
        int counter;
        for (int i = 0; i < n; i++) {
            iloscWystapien[tab[i]]++;
        }
        int j = 0;
        for (int i = 0; i < zakres + 1; i++) {
            counter = iloscWystapien[i];
            while (counter > 0) {
                tab[j] = i;
                counter--;
                j++;
            }
        }
    }
}
