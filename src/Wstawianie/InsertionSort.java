package Wstawianie;

public class InsertionSort {

    public void insertionSort (int[] tab) {
        int n = tab.length;
        int element;
        int j;
        for (int i = 1; i < n; i++) {
            element = tab[i];
            j = i - 1;
            while (j>=0 && tab[j]>element) {
                int temp = tab[j+1];
                tab[j+1] = tab[j];
                tab[j] = temp;
                j--;
            }
        }
    }
}
