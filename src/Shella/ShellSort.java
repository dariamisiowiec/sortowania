package Shella;

public class ShellSort {

    public void shellSort(int[] tab) {
        int numberOfelements = tab.length;
        int gap = numberOfelements/2;
        int firstIndex;
        int endIndex;
        int temp;
        while (gap>=1){
            firstIndex = 0;
            endIndex = gap;
            while (endIndex<numberOfelements) {
                int i = firstIndex;
                int j = endIndex;
                while (i>=0 && tab[i] > tab[j]) {
                    temp = tab[i];
                    tab[i] = tab[j];
                    tab[j] = temp;
                    i = i - gap;
                    j = j - gap;
                }
                endIndex += 1;
                firstIndex += 1;
            }
            gap = gap/2;
        }
    }

}
